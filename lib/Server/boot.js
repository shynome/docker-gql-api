const logger = require('log4js').getLogger()

/**
 * @param {any} module
 */
module.exports = function boot(module){
  if( require.main !== module )return
  module.exports.createServer()
  // @ts-ignore
  .catch(err=>{
    logger.error(err)
  })
}
