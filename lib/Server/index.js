const GraphQLServer = require('./GraphQL')

exports.createServer = async ()=>{
  await Promise.all([
    GraphQLServer.createServer()
  ])
}

require('@/Server/boot')(module)
