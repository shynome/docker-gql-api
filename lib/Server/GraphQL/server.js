const { ApolloServer } = require('apollo-server')
const logger = require('log4js').getLogger('GraphQL')
const http = require('http')

const server = exports.server = new ApolloServer({
  schema: require('./schema').schema,
  /**@param {{ req: http.IncomingMessage }} context */
  context: async (context)=>{
    return {}
  },
  tracing: true,
})

const { GQLPORT } = require('@/Common/config')

exports.createServer = async ()=>{
  
  let serverInfo = await server.listen(GQLPORT)
  logger.info(`Server ready at ${serverInfo.url}`)

}

require('@/Server/boot')(module)
