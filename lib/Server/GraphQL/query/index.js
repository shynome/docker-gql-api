const { GraphQLObjectType, GraphQLString } = require('graphql')

module.exports = new GraphQLObjectType({
  name: 'query',
  fields:{
    "hello": {
      type: GraphQLString,
      resolve: ()=>'world'
    }
  }
})
