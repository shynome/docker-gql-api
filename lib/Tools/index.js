
exports.getEnv =  (name='',required=false, msg = `Env: ${name} is required`)=>{
  if(typeof process.env[name] === 'undefined' && required){
    throw new Error(msg)
  }
  return process.env[name] || ''
}
