const { getEnv } = require('@/Tools')

exports.GQLPORT = getEnv('GQLPORT') || '8080'
