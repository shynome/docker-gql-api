import { Logger } from 'log4js'

declare module 'log4js' {
  export function getLogger(category?: 'GraphQL'|'default'): Logger;
}
