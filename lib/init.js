require('dotenv').config()
require('./Extends')

const { getEnv } = require('./Tools')

const LOG_LEVEL = getEnv('LOG_LEVEL') || 'info'

require('log4js').configure({
  appenders: {
    console: { type: 'console' }
  },
  categories: {
    'default': {
      level: LOG_LEVEL,
      appenders: ['console'],
    },
    'GraphQL': {
      level: LOG_LEVEL,
      appenders: ['console'],
    }
  } 
})
